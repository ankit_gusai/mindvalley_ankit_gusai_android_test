package com.example.ankit.myapplication.ui;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ankit.myapplication.R;
import com.example.ankit.myapplication.network.Handlers.ImageHandler;
import com.example.ankit.myapplication.network.responseModel.PastBeanSample;
import com.example.ankit.myapplication.utilities.Utils;
import com.example.ankit.myapplication.network.NetworkHelper;
import com.example.ankit.myapplication.network.Handlers.JsonHandler;
import com.example.ankit.myapplication.network.responseModel.BingDummyModel;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class MainActivity extends AppCompatActivity {
    private static final String BING_TEST_URL = "http://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=en-US";
    private static final String PASTEBIN_TEST_URL = "http://pastebin.com/raw/wgkJgazE";
    private CompositeSubscription mCompositeSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //we pass 0 te set mem cache size to default
        NetworkHelper.init(0);
        mCompositeSubscription = new CompositeSubscription();
        findViewById(R.id.button).setOnClickListener(v -> testPasteBeanUrl());
        findViewById(R.id.button2).setOnClickListener(v -> ((ImageView) findViewById(R.id.imageView)).setImageBitmap(null));
    }


    //Just a method demonstration
    private void testMark1() {
        Subscription subscription = NetworkHelper
                .getHelper()
                .createRequest()
                .get(BING_TEST_URL)
                .with(JsonHandler.Builder.createDefault(BingDummyModel.class))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BingDummyModel>() {
                    @Override
                    public void onCompleted() {
                        Utils.logd("onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Utils.logd("onError: ");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(BingDummyModel dummyModel) {
                        Toast.makeText(MainActivity.this, "response " + dummyModel.images.get(0).url, Toast.LENGTH_SHORT).show();
                        Utils.logd("onNext: ");
                        Utils.logd("response " + dummyModel.images.get(0).url);
                    }
                });

        mCompositeSubscription.add(subscription);
    }

    //Just a method demonstration
    private void testImageMark1() {
        Subscription subscription =
                NetworkHelper
                        .getHelper()
                        .createRequest()
                        .get("http://www.bing.com/az/hprichbg/rb/Halloween2016_EN-US7682362704_1920x1080.jpg")
                        .with(ImageHandler.Builder.create(200, 200))
                        .subscribe(new Subscriber<Bitmap>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Bitmap bitmap) {
                                ((ImageView) findViewById(R.id.imageView)).setImageBitmap(bitmap);
                            }
                        });
        mCompositeSubscription.add(subscription);
    }


    /**
     * @return Json Response Observable
     */
    private Observable<BingDummyModel> getResponse() {
        return NetworkHelper
                .getHelper()
                .createRequest()
                .get(BING_TEST_URL)
                .with(JsonHandler.Builder.createDefault(BingDummyModel.class));
    }

    /**
     * @param url image url
     * @return Bitmap Response Observable
     */
    private Observable<Bitmap> getBitmapFromResponse(String url) {
        return NetworkHelper
                .getHelper()
                .createRequest()
                .get(url)
                //.with(ImageHandler.Builder.createDefault());
                .with(ImageHandler.Builder.create(200, 200));
    }

    /**
     * Merging two requesting together. simple observable stuff
     */
    private void testCompositeMark2() {
        getResponse()
                .flatMap(dummyModel -> getBitmapFromResponse("http://www.bing.com" + dummyModel.images.get(0).url))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Bitmap>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        ((ImageView) findViewById(R.id.imageView)).setImageBitmap(bitmap);
                    }
                });
    }

    /**
     * Past bean URL sample request fetch and image loading
     */
    private void testPasteBeanUrl() {
        NetworkHelper.getHelper()
                .createRequest()
                .get(PASTEBIN_TEST_URL)
                .with(JsonHandler.Builder.createDefault(PastBeanSample[].class))
                .flatMap(pastBeanSamples -> NetworkHelper.getHelper()
                        .createRequest()
                        .get(pastBeanSamples[0].urls.regular)
                        .with(ImageHandler.Builder.createDefault()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Bitmap>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        ((ImageView) findViewById(R.id.imageView)).setImageBitmap(bitmap);
                    }
                });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeSubscription.unsubscribe();
    }
}
