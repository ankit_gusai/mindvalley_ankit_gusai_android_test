package com.example.ankit.myapplication.network.Handlers;

/**
 * Handler Definition.
 * <p>
 * </p>Handler is abstract module that can be plugged into network request to handle core response.
 * </p>there is mainly two parts.
 * </p>   1)handle byte[] response
 * </p>   2)Manipulate response before cache(do we even require this?)
 * <p>
 * </p>Created by Ankit on 30-10-2016.
 */

public interface Handler<T> {
    /**
     * Handle bytes[] from core Http response.
     *
     * @param bytes bytes[] from http response body
     */
    T handleResponse(byte[] bytes) throws Exception;

    byte[] modifyBeforeCache(byte[] bytes);
}
