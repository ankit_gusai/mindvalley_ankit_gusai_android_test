package com.example.ankit.myapplication.utilities;

import android.util.Log;

import com.example.ankit.myapplication.BuildConfig;

/**
 * Created by Ankit on 31-10-2016.
 */

public class Utils {
    /**
     * Debug log enable tag
     */
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "Network Helper Test";

    public static void logd(String msg) {
        if (DEBUG) {
            Log.d(TAG, msg);
        }
    }
}
