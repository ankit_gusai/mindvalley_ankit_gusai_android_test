package com.example.ankit.myapplication.network.responseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ankit on 02-11-2016.
 */

public class PastBeanSample {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("width")
    @Expose
    public int width;
    @SerializedName("height")
    @Expose
    public int height;
    @SerializedName("color")
    @Expose
    public String color;
    @SerializedName("likes")
    @Expose
    public int likes;
    @SerializedName("liked_by_user")
    @Expose
    public boolean likedByUser;
    @SerializedName("user")
    @Expose
    public User user;
    @SerializedName("current_user_collections")
    @Expose
    public List<Object> currentUserCollections = new ArrayList<Object>();
    @SerializedName("urls")
    @Expose
    public Urls urls;
    @SerializedName("categories")
    @Expose
    public List<Category> categories = new ArrayList<Category>();
    @SerializedName("links")
    @Expose
    public Links__ links;

    public class Category {

        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("photo_count")
        @Expose
        public int photoCount;
        @SerializedName("links")
        @Expose
        public Links_ links;

    }

    public class Links_ {

        @SerializedName("self")
        @Expose
        public String self;
        @SerializedName("photos")
        @Expose
        public String photos;

    }

    public class Links__ {

        @SerializedName("self")
        @Expose
        public String self;
        @SerializedName("html")
        @Expose
        public String html;
        @SerializedName("download")
        @Expose
        public String download;

    }

    public class Links {

        @SerializedName("self")
        @Expose
        public String self;
        @SerializedName("html")
        @Expose
        public String html;
        @SerializedName("photos")
        @Expose
        public String photos;
        @SerializedName("likes")
        @Expose
        public String likes;

    }

    public class ProfileImage {

        @SerializedName("small")
        @Expose
        public String small;
        @SerializedName("medium")
        @Expose
        public String medium;
        @SerializedName("large")
        @Expose
        public String large;

    }

    public class Urls {

        @SerializedName("raw")
        @Expose
        public String raw;
        @SerializedName("full")
        @Expose
        public String full;
        @SerializedName("regular")
        @Expose
        public String regular;
        @SerializedName("small")
        @Expose
        public String small;
        @SerializedName("thumb")
        @Expose
        public String thumb;

    }

    public class User {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("username")
        @Expose
        public String username;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("profile_image")
        @Expose
        public ProfileImage profileImage;
        @SerializedName("links")
        @Expose
        public Links links;

    }
}
