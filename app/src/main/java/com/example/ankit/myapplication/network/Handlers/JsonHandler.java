package com.example.ankit.myapplication.network.Handlers;

import com.google.gson.GsonBuilder;

import java.nio.charset.Charset;

/**
 * Created by Ankit on 30-10-2016.
 */

public class JsonHandler<T> implements Handler<T> {
    private Class<T> mTClass;

    public JsonHandler(Class<T> mTClass) {
        this.mTClass = mTClass;
    }

    @Override
    public T handleResponse(byte[] bytes) {
        String responseBody = new String(bytes, Charset.forName("UTF-8"));
        return new GsonBuilder().create().fromJson(responseBody, mTClass);
    }

    @Override
    public byte[] modifyBeforeCache(byte[] bytes) {
        return bytes;
    }


    public static class Builder<Z> extends HandlerBuilder<JsonHandler<Z>> {
        Class<Z> zClass;

        public static <T> Builder<T> createDefault(Class<T> t) {
            return new Builder<>(t);
        }

        public Builder(Class<Z> zClass) {
            this.zClass = zClass;
        }

        @Override
        public JsonHandler<Z> build() {
            return new JsonHandler<>(zClass);
        }
    }
}
