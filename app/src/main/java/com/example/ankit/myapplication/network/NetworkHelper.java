package com.example.ankit.myapplication.network;

import android.support.v4.util.LruCache;

import com.example.ankit.myapplication.utilities.Utils;
import com.example.ankit.myapplication.network.Handlers.Handler;
import com.example.ankit.myapplication.network.Handlers.HandlerBuilder;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;

/**
 * Simple network helper with plugable response handler. This class in singleton in order for memcache to work efficiently.
 * <p>
 * Created by Ankit on 30-10-2016.
 */

/*
* USAGE
* Usage is pretty straight forward
* 1)Get Network helper object(SingleTon Object)
* 2)create Request
* 3)plug URL to fetch
* 4)plug response handler(Response handlers are pluggable modules that can handle response on their specific way)
* 4)some Rx threading management according to you need.
*
 */

public class NetworkHelper {

    /**
     * SingleTon object
     */
    private static NetworkHelper mNetworkHelper;
    /**
     * In memory cache(RAM)
     */
    private LruCache<String, byte[]> mMemoryCache;
    /**
     * HTTP network client
     */
    private OkHttpClient mOkHttpClient;

    private static final Object LOCK = new Object();

    /**
     * Private constructor
     */
    private NetworkHelper() {
    }

    /**
     * Init necessary stuff.
     *
     * @param memCacheSize in memoey cache size in bytes
     */
    public static void init(long memCacheSize) {
        if (mNetworkHelper == null) {
            mNetworkHelper = new NetworkHelper();
            mNetworkHelper.initCache(memCacheSize);
            mNetworkHelper.initOkHttpClient();
        }
    }

    public static NetworkHelper getHelper() {
        if (mNetworkHelper == null) {
            init(0);
        }
        return mNetworkHelper;
    }

    public LocalRequest createRequest() {
        return new LocalRequest();
    }


    /**
     * For each individual call we create a request.
     * the idea is to make this process concurrent. multiple thread can use this simultaneously( only barring memcache access).
     */
    public class LocalRequest {
        private String key;
        private int consumed = 0;
        private boolean isInProcess;

        public LocalRequest get(String url) {
            this.key = url;
            return this;
        }


        /**
         * Same thing as {{@link #with(Handler)}} method. we follow Builder pattern instead.
         * this is also helpful when building a handler is expensive process. with this build method will be called on worker thread.
         */
        public <Z, T extends Handler<Z>> Observable<Z> with(final HandlerBuilder<T> t) {
            return Observable.just(t.build())
                    .flatMap(this::with);
        }

        /**
         * The handler attachment. this handler will be handed byte array from okHttp response.
         */
        private <Z> Observable<Z> with(final Handler<Z> zHandler) {
            Observable<Z> mObservable = Observable.create(subscriber -> {
                Utils.logd("call: executing");
                if (consumed == 1) {
                    throw new UnsupportedOperationException("Request cannot be reused.");
                }
                isInProcess = true;
                byte[] bytes;
                try {
                    //get Response
                    bytes = getBytesResponse(key);
                    //ask handler to handle it.
                    Z z = zHandler.handleResponse(bytes);
                    //deliver result to subscriber
                    subscriber.onNext(z);
                    //ask handler to modify cache before it gets saved to cache
                    bytes = zHandler.modifyBeforeCache(bytes);
                    //save cache to in memory cache
                    saveToMemCache(key, bytes);
                    //We are don with the process.
                    subscriber.onCompleted();

                } catch (Exception e) {
                    Utils.logd("oop! not good");
                    subscriber.onError(e);
                }

                consumed = 1;
                isInProcess = false;

            });

            mObservable.doOnUnsubscribe(() -> {
                if (isInProcess) {
                    cancelCallWithTag(mOkHttpClient, key);
                }
            });

            return mObservable;

        }

        /**
         * get response from memory, if not present in memory, call network.
         */
        private byte[] getBytesResponse(String key) throws IOException {
            byte[] bytes = getFromCache(key);
            if (bytes == null) {
                Utils.logd("Cache not available, fetching from network");
                bytes = getFromNetwork(key);
            }

            return bytes;
        }


        private void cancelCallWithTag(OkHttpClient client, String tag) {
            // A call may transition from queue -> running. Remove queued Calls first.
            for (Call call : client.dispatcher().queuedCalls()) {
                if (call.request().tag().equals(tag))
                    call.cancel();
            }
            for (Call call : client.dispatcher().runningCalls()) {
                if (call.request().tag().equals(tag))
                    call.cancel();
            }
        }
    }

    private synchronized byte[] getFromCache(String key) {
        return mMemoryCache.get(key);
    }

    //saves bytes to cache
    private synchronized void saveToMemCache(String key, byte[] bytes) {
        if (mMemoryCache.get(key) == null) {
            mMemoryCache.put(key, bytes);
        }
    }

    /**
     * Standard OKHttp call.
     */
    private byte[] getFromNetwork(String key) throws IOException {
        Request request = new Request.Builder()
                .url(key)
                .tag(key)
                .build();

        Response response = mOkHttpClient.newCall(request).execute();

        //TODO this is just speculation. we need to test this
        long freeMemory = Runtime.getRuntime().freeMemory();
        if (response.body().contentLength() > freeMemory) {
            throw new IOException("Not enough Memory to hold response in-memory");
        }

        return response.body().bytes();
    }

    //Initialize the in memory cache.
    private void initCache(long size) {
        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        long maxMem = Runtime.getRuntime().maxMemory();
        if (size >= maxMem) {
            throw new UnsupportedOperationException("Mem cache size cannot exceed allowed heap size");
        }

        int cacheSize = (int) size;

        // We set default cache as 1/8th of available heap
        if (size == 0) {
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            // Use 1/8th of the available memory for this memory cache.
            cacheSize = maxMemory / 8;

        }

        mMemoryCache = new LruCache<String, byte[]>(cacheSize) {
            @Override
            protected int sizeOf(String key, byte[] value) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return value.length / 1024;
            }
        };
    }

    /**
     * Simple OkHttp reference creation.
     */
    private void initOkHttpClient() {
        mOkHttpClient = new OkHttpClient();
    }

}
