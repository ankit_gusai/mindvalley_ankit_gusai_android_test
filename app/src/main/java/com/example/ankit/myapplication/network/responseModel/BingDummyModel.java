package com.example.ankit.myapplication.network.responseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ankit on 30-10-2016.
 */

public class BingDummyModel {

    @SerializedName("images")
    @Expose
    public List<Image> images = new ArrayList<Image>();

    public class Image {

        @SerializedName("startdate")
        @Expose
        public String startdate;
        @SerializedName("enddate")
        @Expose
        public String enddate;
        @SerializedName("url")
        @Expose
        public String url;

    }
}
