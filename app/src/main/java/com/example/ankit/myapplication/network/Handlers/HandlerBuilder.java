package com.example.ankit.myapplication.network.Handlers;

/**
 * Handler Builder Definition.
 * <p>
 * </p> Created by Ankit on 30-10-2016.
 */

public abstract class HandlerBuilder<T extends Handler> {
    public abstract T build();
}
