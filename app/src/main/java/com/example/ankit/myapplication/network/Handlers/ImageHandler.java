package com.example.ankit.myapplication.network.Handlers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by Ankit on 31-10-2016.
 */

public class ImageHandler implements Handler<Bitmap> {
    int height;
    int width;

    public ImageHandler(int height, int width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public Bitmap handleResponse(byte[] bytes) {
        //default creation. this doesn't do any scale.
        if (height == 0 || width == 0) {
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }
        //implement scaling
        return decodeSampledBitmap(bytes, width, height);
    }

    @Override
    public byte[] modifyBeforeCache(byte[] bytes) {
        return bytes;
    }

    private Bitmap decodeSampledBitmap(byte[] bytes, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
    }

    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static class Builder extends HandlerBuilder<ImageHandler> {
        int height;
        int width;

        public static Builder createDefault() {
            return new Builder();
        }

        public static Builder create(int height, int width) {
            return new Builder(height, width);
        }

        public Builder() {

        }

        public Builder(int height, int width) {
            this.height = height;
            this.width = width;
        }

        @Override
        public ImageHandler build() {
            return new ImageHandler(height, width);
        }
    }
}
